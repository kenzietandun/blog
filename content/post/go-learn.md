---
title: "Go Learn"
date: 2019-11-26T20:33:43+13:00
draft: true
image: 'image/go-learn.png'
description: 'Download resources from LEARN automagically, powered by Golang'
disableComments: true
---

go-Learn is a webscraper written in Golang to download resources from the LEARN website of University of Canterbury. Instead of checking LEARN everyday, one can utilise go-Learn to find new resources released by the course and get it automatically. It uses SQLite on the backend to keep track of the downloaded files to prevent re-downloading and speed up scraping. Go-Learn also supports finding resources from individual courses to save time parsing the whole website.

Github page: https://gitlab.com/kenzietandun/go-learn

Download: https://go-learn.zies.net
